# Pre-commit hooks

Collection of pre-commit hooks to be used with [Yelp's](http://pre-commit.com/) pre-commit framework.

## Available Hooks

### Terraform

* Terraform format
* Terraform validate

### Golang

* Golang fmt (requires golang)
* Golang lint (requires [https://github.com/golang/lint](https://github.com/golang/lint))
* Golang vet (requires golang)
* Golang metalinter
* Golang validate toml (requires [https://github.com/BurntSushi/toml/tree/master/cmd/tomlv](https://github.com/BurntSushi/toml/tree/master/cmd/tomlv))

### Helm

* Helm lint (requires Helm)

### Ansible

* Ansible lint (requires ansible-lint)

### Prometheus

* Prometheus rules and configuration validation

Check the [hooks manifest](https://github.com/kintoandar/pre-commit/blob/master/.pre-commit-hooks.yaml) to know more.

## How-To

### Step 1

Install the pre-commit package

```shell
brew install pre-commit
```

_For operating systems, other than macOS, check the [official documentation](http://pre-commit.com/#install)_

### Step 2

Step into the repo you want to have the pre-commit hooks installed and run:

#### For Terraform

```shell
cat <<EOF > .pre-commit-config.yaml
- repo: https://gitlab.com/adriagalin/pre-commit-hooks.git
  rev: master
  hooks:
  - id: terraform_fmt
  - id: terraform_validate
- repo: git://github.com/pre-commit/pre-commit-hooks
  rev: v3.1.0
  hooks:
    - id: check-added-large-files
    - id: check-json
    - id: detect-private-key
    - id: check-yaml
    - id: end-of-file-fixer
    - id: trailing-whitespace
EOF
```

#### For Golang

```shell
cat <<EOF > .pre-commit-config.yaml
- repo: https://gitlab.com/adriagalin/pre-commit-hooks.git
  rev: master
  hooks:
  - id: go-fmt
  - id: go-vet
  - id: go-lint
  - id: validate-toml
  - id: gometalinter
- repo: git://github.com/pre-commit/pre-commit-hooks
  rev: v3.1.0
  hooks:
    - id: check-added-large-files
    - id: check-json
    - id: detect-private-key
    - id: check-yaml
    - id: end-of-file-fixer
    - id: trailing-whitespace
EOF
```

#### For Helm

```shell
cat <<EOF > .pre-commit-config.yaml
- repo: https://gitlab.com/adriagalin/pre-commit-hooks.git
  sha: master
  hooks:
  - id: helm-lint
- repo: git://github.com/pre-commit/pre-commit-hooks
  rev: v3.1.0
  hooks:
    - id: check-added-large-files
    - id: check-json
    - id: detect-private-key
    - id: check-yaml
    - id: end-of-file-fixer
    - id: trailing-whitespace
EOF
```

#### For Ansible

```shell
cat <<EOF > .pre-commit-config.yaml
- repo: https://github.com/willthames/ansible-lint.git
  rev: v4.2.0
  hooks:
    - id: ansible-lint
      files: \.(yaml|yml)$
      args: [
        "--exclude=external_roles",
        "--exclude=roles",
        "--exclude=basic/roles",
        "--exclude=.travis.yml"
      ]
- repo: git://github.com/pre-commit/pre-commit-hooks
  rev: v3.1.0
  hooks:
    - id: check-added-large-files
    - id: check-json
    - id: detect-private-key
    - id: check-yaml
    - id: end-of-file-fixer
    - id: trailing-whitespace
EOF
```

### Step 3

Install the pre-commit hook

```shell
pre-commit install
```

## Credits

Extremely based on [kintoandar](https://blog.kintoandar.com) and [dnephin](https://github.com/dnephin/pre-commit-golang) work.
