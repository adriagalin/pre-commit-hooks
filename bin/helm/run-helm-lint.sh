#!/usr/bin/env bash
set -e
for file in "$@"; do
    helm lint $file
done
